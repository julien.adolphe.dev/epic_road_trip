const express = require("express");
const axios = require("axios");

require("dotenv").config();

const GOOGLE_MAPS_API = process.env.GOOGLE_MAPS_API;
const PORT = 8004;
const HOST = "0.0.0.0";

const app = express();

app.get("/", (req, res) => {
  res.send(
    `Hello from MAPS MICROSERVICE !!!!!!!!WESH!!!!!!!!!!!!!!! ${GOOGLE_MAPS_API}\n `
  );
});

app.get("/give-me-road", (req, res) => {
  const route = axios
    .get(
      `https://maps.googleapis.com/maps/api/directions/json?origin=Toulouse+France&destination=carcassonne+France&key=${GOOGLE_MAPS_API}`
    )
    .then((res) => {
      console.log(res.data);
    })
    .catch((e) => {
      throw e;
    });

  res.send(route);
  // Ask google Direction API

  // Then send the result
  // res.send("Hello from MAPS MICROSERVICE \n");
});

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});
