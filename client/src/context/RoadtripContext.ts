import * as React from "react";
import { TActivity, TStep, TUserData } from "../App";

interface IRoadTripContext {
  setUserData:(e: TUserData) => void;
  handleUpdateUserData: (newVal: TUserData | undefined) => void;
  userData: TUserData | undefined;
  handleUpdateStepArray: (e: TStep[]) => void;
  stepArray: TStep[];
  handleUpdateActivity: (newVal: TActivity) => void;
  activity: TActivity;
  handleUpdateArrayActivity: (newVal: TActivity) => void;
  arrayActivity: TActivity[];
  handleUpdateStep: (e: TStep) => void;
  step: TStep;
  indexStep: number;
  setIndexStep: (newVal: number) => void;
}

export const RoadtripContext = React.createContext<IRoadTripContext>({
  setUserData:(e:TUserData) => {},
  handleUpdateUserData: (e: TUserData | undefined) => {},
  userData: {},
  handleUpdateStepArray: (e: TStep[]) => {},
  stepArray: [],
  handleUpdateActivity: (newVal: TActivity) => {},
  activity: {},
  handleUpdateArrayActivity: (newVal: TActivity) => {},
  arrayActivity: [],
  handleUpdateStep: (e: TStep) => {},
  step: {},
  indexStep: -1,
  setIndexStep: (newval: number) => {},
});
