import * as React from "react";
import luggage from "../ressources/luggage.jpg"
import "./header.scss";

const Header: React.FC = () => {
  return (
    <div className="header">
     <img className="header-image" src={luggage} alt="luggage"/>
    </div>
  );
};

export default Header;
