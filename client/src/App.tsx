import {useState} from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import "./app.css";
import Content from "./Content/Content";
import Register from "./Content/Connection/Register";
import { RoadtripContext } from "./context/RoadtripContext";
import Login from "./Content/Connection/Login";

export type TUserData = {
  userId?: string;
  creationDate?: string;
  roadTripId?: number
  name?: string
  description?: string
  data?: TStep[]
  _id ? : string
} 
export type TStep = {
  idCity?: number;
  city?: string;
  activity?: TActivity[];
};
export type TActivity = {
  type?: string;
  place?: string;
  id?: string;
  location?: { [key: string]: string };
};

const App: React.FC = () => {
  //final object, data to send to api
  const [userData, setUserData] = useState<TUserData | undefined>()
  const handleUpdateUserData = (newVal:TUserData | undefined):void => {
    setUserData(newVal)
  }

  //roadtrip object
  const [stepArray, setStepArray] = useState<TStep[]>([]);
  const handleUpdateStepArray = (newVal: TStep[]): void => {
    setStepArray(newVal);
  };

  //object with city
  const [step, setStep] = useState<TStep>({});
  const handleUpdateStep = (newVal: TStep): void => {
    setStep(newVal);
  };

  //array of activity
  const [arrayActivity, setArrayActivity] = useState<TActivity[]>([]);
  const handleUpdateArrayActivity = (newVal: TActivity) => {
    let newArray: TActivity[] = arrayActivity;
    newArray.push(newVal);
    setArrayActivity(newArray);
  };

  //todo INDEX STEP
  const [indexStep, setIndexStep] = useState<number>(-1);

  // object with activity
  const [activity, setActivity] = useState<TActivity>({});
  const handleUpdateActivity = (newVal: TActivity): void => {
    //!! newVal doit d'abord contenir ce qu'il y avait dans activity
    setActivity(newVal);
  };

  return (
    <RoadtripContext.Provider
      value={{
        setUserData,
        handleUpdateUserData,
        userData,
        handleUpdateStepArray,
        stepArray,
        handleUpdateActivity,
        activity,
        handleUpdateArrayActivity,
        arrayActivity,
        handleUpdateStep,
        step,
        indexStep,
        setIndexStep,
      }}
    >
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Content} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/activity/:tab" component={Content} />
            <Route path="*">
              <div>Oups page introuvable</div>
            </Route>
          </Switch>
        </BrowserRouter>
      </div>
    </RoadtripContext.Provider>
  );
};

export default App;
