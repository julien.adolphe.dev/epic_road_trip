import { useEffect, useState } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Grid,
  DialogActions,
  Button,
  Card,
  CardHeader,
  CardContent,
  Divider,
  Typography,
  CardActions,
} from "@material-ui/core";
// import ImportExportIcon from "@material-ui/icons/ImportExport";
// import ShareIcon from "@material-ui/icons/Share";
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import "./myTrips.scss";
import { TUserData } from "../../App";
import { IMyTrips } from "./IMytrips";

const MyTrips: React.FC<IMyTrips> = ({ open, setOpenTrips }) => {
  const [tripUser, setTripUser] = useState<TUserData[]>();

  useEffect(() => {
    const userId: AxiosRequestConfig | undefined = localStorage.getItem(
      "idUser"
    ) as AxiosRequestConfig | undefined;
    async function getTrip() {
      await axios
        .get(`http://localhost:8000/api/v1/trip/user/${userId}`)
        .then((response: AxiosResponse<any>) => {
          setTripUser(response.data);
        })
        .catch((err) => {
          window.alert("Une erreur est arrivée");
        });
    }
    getTrip();
  }, []);

  async function deleteTrip(tripId: any) {
    await axios
      .delete(`http://localhost:8000/api/v1/trip/delete/${tripId}`)
      .then((response: AxiosResponse<any>) => {
        console.log(`response`, response);
        setOpenTrips(false);
      })
      .catch((err) => {
        window.alert("Une erreur est arrivée");
      });
  }

  return (
    <Dialog
      onClose={() => setOpenTrips(false)}
      open={open}
      fullWidth
      maxWidth={"lg"}
      className="myTrips"
    >
      <DialogTitle>
        <Typography
          variant="h3"
          color="textSecondary"
          component="h1"
          className="myTrips-title"
        >
          Your trips
        </Typography>
      </DialogTitle>
      <DialogContent className="myTrips-content">
        <Grid container justify="center" direction="column">
          <Grid item className="myTrips-content-list">
            {tripUser &&
              tripUser.map((dataTrip: TUserData, index) => {
                console.log(`dataTrip`, dataTrip);
                return (
                  <ul>
                    <Card key={index} className="cardTrip">
                      <CardHeader title={`Name: ${dataTrip.name}`} />
                      <CardContent>
                        <li>Creation date : {dataTrip.creationDate}</li>
                        <li>Description : {dataTrip.description}</li>
                        {dataTrip.data &&
                          dataTrip.data.map((step, index) => {
                            return (
                              <>
                                <Card key={index} className="cardStep">
                                  <CardHeader 
                                    title={`Step ${index + 1} : ${step.city}`}
                                  ></CardHeader>
                                  <Divider />
                                  <CardContent>
                                    {step.activity &&
                                      step.activity.map((activityStep) => {
                                        return (
                                          <>
                                            <li>Type of activity: {activityStep.type}</li>
                                            <li>Place: {activityStep.place}</li>
                                            <Divider />
                                          </>
                                        );
                                      })}
                                  </CardContent>
                                </Card>
                              </>
                            );
                          })}
                      </CardContent>
                      <CardActions>
                        {/* <Button
                          variant="contained"
                          color="primary"
                          className="myTrips-footer-button"
                          startIcon={<ImportExportIcon />}
                          onClick={() => {
                            window.print();
                          }}
                        >
                          Export this trip
                        </Button> */}
                        <Grid container justify="flex-end">
                          <Button
                            variant="contained"
                            color="secondary"
                            className="myTrips-footer-button"
                            onClick={() => deleteTrip(dataTrip._id)}
                          >
                            Delete
                          </Button>
                        </Grid>
                      </CardActions>
                    </Card>
                  </ul>
                );
              })}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions className="myTrips-footer">
        <Grid container justify="center">
          <Button
            variant="outlined"
            color="secondary"
            className="myTrips-footer-button"
            onClick={() => {
              setOpenTrips(false);
              localStorage.removeItem("id_trip");
            }}
          >
            Close
          </Button>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};

export default MyTrips;
