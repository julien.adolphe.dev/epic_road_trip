export interface IMyTrips {
  open: boolean;
  setOpenTrips: React.Dispatch<React.SetStateAction<boolean>>;
}
