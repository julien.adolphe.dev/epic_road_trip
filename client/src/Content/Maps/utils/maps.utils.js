import { Loader } from "@googlemaps/js-api-loader";
import { flatten } from "lodash";
import drink from "../../../ressources/drink-icon.png";
import eat from "../../../ressources/eat-icon.png";
import travel from "../../../ressources/travel-icon.png";
import sleep from "../../../ressources/sleep-icon.png";
import enjoy from "../../../ressources/enjoy-icon.png";

const dotenv = require("dotenv");
dotenv.config();
// const env_react = process.env.REACT_APP_GOOGLE_MAPS_API;

// Sort the object to get arrays
export function sortStepArray(stepArray) {
  if (stepArray.length === 0) {
    return { waypoint: [], activity: [] };
  }

  const step = stepArray.map((stepEl) => {
    return stepEl.city;
  });

  const isThereActivity = stepArray.map((act) =>
    act.hasOwnProperty("activity")
  );

  if (!isThereActivity.includes(true)) {
    return { waypoint: step, activity: [] };
  }
  const activity = flatten(
    stepArray
      .map((stepEl) => {
        return stepEl.activity;
      })
      .filter((item) => typeof item !== "undefined")
  );

  return { waypoint: step, activity };
}

// Load Google Maps Libraries into browser
export function googleMapsLoader() {
  return new Loader({
    apiKey: "AIzaSyBUpar0aWnQx189G-kKaJEDwuWashsoV_Y",
    version: "weekly",
    libraries: ["places"],
  });
}

// Display simple map zoom: number, center: {lat: number, lng: number}
export function displayGoogleMaps(zoom, center) {
  return new window.google.maps.Map(document.getElementById("map"), {
    zoom: zoom,
    center: center,
  });
}
// Display simple map zoom: number, center: {lat: number, lng: number}
export function displayGoogleMapsSummary(zoom, center) {
  return new window.google.maps.Map(document.getElementById("mapSummary"), {
    zoom: zoom,
    center: center,
  });
}

// Export activity icons
export function getIcons(type) {
  // Switch Drinks, eat, sleep, travel, enjoy
  let icon;
  switch (type) {
    case "eat":
      icon = eat;
      break;
    case "drink":
      icon = drink;
      break;
    case "travel":
      icon = travel;
      break;
    case "sleep":
      icon = sleep;
      break;
    case "enjoy":
      icon = enjoy;
      break;
    default:
      break;
  }
  return icon;
}

// Add marker on map
export async function addMarker(map, content, contentType) {
  let icon = getIcons(contentType);
  console.log(`content.location`, content.location);
  if (content.location) {
    const template = `<div>
    <img src=${icon} alt="Type d'activité">
    <h3>${content.place}</h3>
    <div>
        <p>${content.location.formattedAddress[0]}<br>
        ${content.location.formattedAddress[1]}<br>
        ${content.location.formattedAddress[2]}</p>
        <a href="https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(
          content.place +
            " " +
            content.location.formattedAddress[0] +
            " " +
            content.location.formattedAddress[1]
        )}" target="_blank" rel="noreferrer">Voir sur Google Maps</a>
    </div>
</div> `;

    const myLatLng = { lat: content.location.lat, lng: content.location.lng };
    const infowindow = new window.google.maps.InfoWindow({
      content: template,
    });

    const marker = new window.google.maps.Marker({
      position: myLatLng,
      map,
      icon,
    });

    marker.addListener("click", () => {
      infowindow.open({
        anchor: marker,
        map,
        shouldFocus: false,
      });
    });
  }
}

// Display Google Maps + Route from Array of address [address: string]
export async function displayRoute(route, map) {
  const directionsService = new window.google.maps.DirectionsService();
  const directionsRenderer = new window.google.maps.DirectionsRenderer();

  let origin;
  let destination;
  let waypoints = [];
  // let center = { lat: 43.604652, lng: 1.444209 }; // TOULOUSE PUTAING CON

  if (route.length === 1) {
    await findGeoCoords(route[0]);
  }
  if (route.length > 0) {
    origin = route[0];
  }
  if (route.length >= 2) {
    destination = route[route.length - 1];
  }
  if (route.length >= 3) {
    waypoints = route.slice(1, route.length - 1);
  }
  // const map = displayGoogleMaps(7, center);

  directionsRenderer.setMap(map);

  if (route.length >= 2) {
    directionsService.route(
      {
        origin: {
          query: origin,
        },
        destination: {
          query: destination,
        },
        waypoints: waypoints.map((waypoint) => {
          return { location: waypoint, stopover: true };
        }),
        optimizeWaypoints: false,
        travelMode: window.google.maps.TravelMode.DRIVING,
      },
      (response, status) => {
        if (status === "OK") {
          directionsRenderer.setDirections(response);
        } else {
          console.log("Directions request failed due to " + status);
        }
      }
    );
  }
}

// Give {lat: number, lng: number} from an address: string
export async function findGeoCoords(address) {
  const geocoder = new window.google.maps.Geocoder();
  const promise = new Promise((resolve, reject) => {
    geocoder.geocode({ address: address }, (results, status) => {
      if (status === "OK") {
        const result = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng(),
        };
        resolve(result);
      } else {
        console.log(status);
        reject(status);
        throw status;
      }
    });
  });

  const result = promise.then((e) => e);

  return result;
}
