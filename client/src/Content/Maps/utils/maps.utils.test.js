import { sortStepArray } from "./maps.utils";

const mock = [
  {
    city: "Toulouse",
    idCity: 0,
    activity: [
      {
        id: "344247368443582",
        type: "drinks",
        place: "Le bar Basque",
      },
      {
        id: "5327927482734947",
        type: "eat",
        place: "Mc Do",
      },
    ],
  },
  {
    city: "Tarbes",
    idCity: 1,
    activity: [
      {
        id: "413244382578523",
        type: "sleep",
        place: "Ibis hotel",
      },
      {
        id: "4807589723878947",
        type: "eat",
        place: "Flunch",
      },
    ],
  },
];

const ResultMock = {
  waypoint: ["Toulouse", "Tarbes"],
  activity: [
    {
      id: "344247368443582",
      type: "drinks",
      place: "Le bar Basque",
    },
    {
      id: "5327927482734947",
      type: "eat",
      place: "Mc Do",
    },
    {
      id: "413244382578523",
      type: "sleep",
      place: "Ibis hotel",
    },
    {
      id: "4807589723878947",
      type: "eat",
      place: "Flunch",
    },
  ],
};

test("Sort the object to get arrays", async () => {
  const data = sortStepArray(mock);
  expect(data).toMatchObject(ResultMock);
});
