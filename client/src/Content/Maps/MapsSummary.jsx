import { useContext, useEffect } from "react";
import { RoadtripContext } from "../../context/RoadtripContext";
import {
  findGeoCoords,
  googleMapsLoader,
  displayRoute,
  addMarker,
  sortStepArray,
  displayGoogleMapsSummary,
} from "./utils/maps.utils.js";
import useWindowDimensions from "../utils/useWindowDimensions";

// Mock content
// const content = {
//   id: "4ea877f893adf5b1fbbcf005",
//   location: {
//     address: "1 rue Marthe Varsi",
//     cc: "FR",
//     city: "Toulouse",
//     country: "France",
//     formattedAddress: ["1 rue Marthe Varsi", "31300 Toulouse", "France"],
//   },
//   labeledLatLngs: {
//     label: "display",
//     lat: 43.599245933843896,
//     lng: 1.4291465867373512,
//   },
//   lat: 43.599245933843896,
//   lng: 1.4291465867373512,
//   postalCode: "31300",
//   state: "Midi-Pyrénées",
//   name: "The Dispensary",
// };

export default function MapsSummary() {
  const { stepArray, handleUpdateStepArray } = useContext(RoadtripContext);
  let screen = useWindowDimensions();

  const loader = googleMapsLoader();
  // const origin = "Toulouse";
  // const destination = "Epitech Toulouse";
  // let stepList = [
  //   "Agen",
  //   "Bordeaux",
  //   "Brest",
  //   "caen",
  //   "Lille",
  //   "Strasbourg",
  //   "Genève",
  // ];

  // TEST CASES
  // const roadTrip = [origin, ...stepList, destination];
  // const roadTrip = [origin];
  // const roadTrip = [origin, destination];
  // const roadTrip = [origin, "Bordeaux", destination];
  // const roadTrip = [];

  // We have to call all Google Maps function into the loader in order to use the library loaded in the window browser
  const load = () => {
    loader
      .load()
      .then(async () => {
        // console.log("LOAD", stepArray);
        const roadTrip = await sortStepArray(stepArray).waypoint;
        // console.log("roadTrip dans le load", roadTrip);
        const activities = sortStepArray(stepArray).activity;
        // console.log(`activities dans le load`, activities);
        // Map initialisation
        const map = displayGoogleMapsSummary(4, {
          lat: 43.604652,
          lng: 1.444209,
        });

        // Display route
        displayRoute(roadTrip, map);

        // GeoCoding test
        await findGeoCoords("Toulouse");

        // Drink Marker example with mock content
        // await addMarker(map, content, "drink");
        activities.map(async (acti) => {
          if (activities) {
            await addMarker(map, acti, acti.type);
          }
        });
      })
      .catch((e) => {
        console.log(e);
        return;
      });
  };

  useEffect(() => {
    handleUpdateStepArray(stepArray);
    load();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepArray, handleUpdateStepArray]);

  return (
    <>
      <div id="mapSummary" style={{ width: "100%", height: "600px" }}></div>
    </>
  );
}
