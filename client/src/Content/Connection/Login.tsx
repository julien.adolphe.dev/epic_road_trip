import React, { useState, useContext } from "react";
import { Button, Dialog, Grid, TextField } from "@material-ui/core";
import axios, { AxiosResponse } from "axios";
import { ILogin } from "./interfaces";
import { RoadtripContext } from "../../context/RoadtripContext";

const Login: React.FC<ILogin> = ({ open, setOpenLogin }) => {

  const {
    userData,
    handleUpdateUserData,
  } = useContext(RoadtripContext);
  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const insertData = async () => {
    await axios
      .post(`http://localhost:8000/api/v1/user/login`, user)
      .then((response: AxiosResponse<any>) => {
        window.alert("Connexion réussie");
        localStorage.setItem("idUser", response.data.user_id);
        let temp = {...userData, userId: response.data.user_id }
        handleUpdateUserData(temp)
      })
      .catch((err) => {
        window.alert("Une erreur est arrivée");
      });
  };
  return (
    <Dialog
      className="containerList"
      open={open}
      onClose={() => setOpenLogin(false)}
      fullWidth
      maxWidth={"md"}
    >
      <Grid
        container
        justify="center"
        direction="column"
        className="dialog-item"
      >
        <Grid item className="dialog-item-title">
          <h2>Login</h2>
        </Grid>
        <Grid item>
          <Grid container direction="column" alignContent="center">
            <TextField
              className="dialog-item-fieldConnection"
              variant="outlined"
              id="standard-basic"
              label="Email"
              type="text"
              value={user.email}
              onChange={(e) =>
                setUser({ ...user, email: e.target.value })
              }
            />
            <TextField
              className="dialog-item-fieldConnection"
              variant="outlined"
              id="standard-basic"
              label="Password"
              type="password"
              value={user.password}
              onChange={(e) =>
                setUser({ ...user, password: e.target.value })
              }
            />
          </Grid>
        </Grid>
        <Grid item>
          <Grid container justify="center" className="dialog-item-buttonsConnection">
            <Button
              variant="contained"
              color="secondary"
              className="button"
              onClick={() => setOpenLogin(false)}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              className="button"
              onClick={() => {
                insertData();
                setOpenLogin(false);
              }}
            >
              Login
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Dialog>
  );
};

export default Login;
