export interface ILogin {
    open: boolean
    setOpenLogin: React.Dispatch<React.SetStateAction<boolean>>
}

export interface IRegister {
    open: boolean
    setOpenRegister: React.Dispatch<React.SetStateAction<boolean>>
}