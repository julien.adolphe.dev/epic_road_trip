import React, { useState, useContext } from "react";
import { Button, Dialog, Grid, TextField } from "@material-ui/core";
import axios, { AxiosResponse } from "axios";
import { IRegister } from "./interfaces";
import "./connection.scss";
import { RoadtripContext } from "../../context/RoadtripContext";

const Register: React.FC<IRegister> = ({ open, setOpenRegister }) => {
  const { userData, handleUpdateUserData } = useContext(RoadtripContext);
  const [user, setUser] = useState({
    name: "",
    email: "",
    password: "",
  });

  const insertData = async () => {
    await axios
      .post(`http://localhost:8000/api/v1/user/register`, user)
      .then((response: AxiosResponse<any>) => {
        window.alert("Inscription réussie");
        localStorage.setItem("idUser", response.data.user_id);
        let temp = { ...userData, userId: response.data.user_id };

        handleUpdateUserData(temp);
      })
      .catch((err) => {
        window.alert("Une erreur est arrivée");
      });
  };
  return (
    <Dialog
      className="dialog"
      open={open}
      onClose={() => setOpenRegister(false)}
      fullWidth
      maxWidth={"md"}
    >
      <Grid
        container
        justify="center"
        direction="column"
        className="dialog-item"
      >
        <Grid item className="dialog-item-title">
          <h2>Sign up</h2>
        </Grid>
        <Grid item>
          <Grid container direction="column" alignContent="center">
            <TextField
              variant="outlined"
              className="dialog-item-fieldConnection"
              id="standard-basic"
              label="Name"
              type="text"
              value={user.name}
              onChange={(e) => setUser({ ...user, name: e.target.value })}
            />

            <TextField
              className="dialog-item-fieldConnection"
              variant="outlined"
              id="standard-basic"
              label="Email"
              type="text"
              value={user.email}
              onChange={(e) => setUser({ ...user, email: e.target.value })}
            />

            <TextField
              className="dialog-item-fieldConnection"
              variant="outlined"
              id="standard-basic"
              label="Password"
              type="password"
              value={user.password}
              onChange={(e) => setUser({ ...user, password: e.target.value })}
            />
          </Grid>
        </Grid>
        <Grid item>
          <Grid
            container
            justify="center"
            className="dialog-item-buttonsConnection"
          >
            <Button
              className="dialog-item-buttonsConnection"
              variant="contained"
              color="secondary"
              onClick={() => setOpenRegister(false)}
            >
              Cancel
            </Button>
            <Button
              className="dialog-item-buttonsConnection"
              variant="contained"
              color="primary"
              onClick={() => {
                insertData();
                setOpenRegister(false);
              }}
            >
              Signup
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Dialog>
  );
};

export default Register;
