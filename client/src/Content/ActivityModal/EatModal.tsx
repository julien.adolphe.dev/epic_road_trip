import { useState, useContext, useEffect } from "react";
import {
  IconButton,
  Icon,
  List,
  ListItem,
  ListItemText,
  Grid,
  Collapse,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  Link,
} from "@material-ui/core";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import "./activityModal.scss";
import axios, { AxiosResponse } from "axios";
import { RoadtripContext } from "../../context/RoadtripContext";
import { TActivity, TStep } from "../../App";
import { IActivityModal } from "./IActivityModal";
import RecommandationsCard from "./RecommandationsCard/RecommandationsCard";

const EatModal: React.FC<IActivityModal> = ({
  setStateModal,
  handleShowMenu,
}) => {
  const [open, setOpen] = useState<number>();
  const [eatData, setEatData] = useState<any[]>([]);
  const [recommandation, setRecommandation] = useState<
    { [key: string]: string }[]
  >([]);
  const { step, stepArray, indexStep, handleUpdateStepArray } =
    useContext(RoadtripContext);

  useEffect(() => {
    // fetch all eat datas
    const fetchData = async () =>
      await axios
        .get(
          `http://localhost:8002/api/v1/eat/restaurants/${stepArray[indexStep].city}`
        )
        .then((result) => {
          setEatData(result.data);
        });

    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step.city]);

  useEffect(() => {
    if (eatData.length > 0) {
      let recommandationsId: string[] = [];
      let temp: any[] = [];
      //get 3 id of the list
      for (let i = 0; i < 3; i++) {
        recommandationsId.push(eatData[i].id);
      }
      // get all details of 3, for recommandations
      const fetchReco = async () => {
        await Promise.all(
          recommandationsId.map(async (el, i): Promise<any> => {
            await axios
              .get(`http://localhost:8002/api/v1/eat/restaurant/${el}`)
              .then((result: AxiosResponse<any>) => {
                temp.push(result.data);
              });
          })
        ).then(() => {
          setRecommandation(temp);
        });
      };
      fetchReco();
    }
  }, [eatData]);

  //get details of the choosen bar
  const [eatDetail, setEatDetail] = useState<any>();
  async function handleEatData(id: string) {
    await axios
      .get(`http://localhost:8002/api/v1/eat/restaurant/${id}`)
      .then((result: AxiosResponse<any>) => {
        setEatDetail(result.data);
      });
  }

  const handleClick = (key: number) => {
    setOpen(key);
  };
  //Update the activity array
  function updateActivity(val: TActivity) {
    let stepToModified: TStep = stepArray[indexStep];
    if (stepToModified.activity) {
      let activityArray: TActivity[] = stepToModified.activity;
      activityArray.push(val);
      stepToModified = { ...stepToModified, activity: activityArray };
    } else {
      stepToModified = { ...stepToModified, activity: [val] };
    }
    let newStepArray = stepArray;
    newStepArray[indexStep] = stepToModified;
    handleUpdateStepArray(newStepArray);
  }

  return (
    <>
      <DialogContent className="activityPopUp">
        <RecommandationsCard
          recommandation={recommandation}
          updateActivity={updateActivity}
          type="eat"
          handleShowMenu={handleShowMenu}
          setStateModal={setStateModal}
        />

        <Grid item lg={8} md={8} sm={8} className="activityPopUp-drinkList">
          <List>
            {eatData.map((data, i) => {
              return (
                <div key={i}>
                  <ListItem
                    divider
                    button
                    onClick={() => {
                      handleClick(i);
                      handleEatData(data.id);
                    }}
                  >
                    <ListItemText primary={data.name} />
                    {open ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={open === i} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      <ListItem className="activityPopUp-detail">
                        {eatDetail && (
                          <>
                            <ul>
                              {eatDetail.location.formattedAddress.map(
                                (address: any, i: number) => {
                                  return (
                                    <>
                                      <Typography
                                        key={i}
                                        variant="body2"
                                        color="textSecondary"
                                        component="p"
                                      >
                                        <li>{address}</li>
                                      </Typography>
                                    </>
                                  );
                                }
                              )}
                            </ul>
                            <Typography
                              variant="body2"
                              color="textSecondary"
                              component="p"
                            >
                              <ul>
                                <li>Category: {eatDetail.categoryName}</li>
                                {eatDetail.phoneNumber && (
                                  <li>Phone number: {eatDetail.phoneNumber}</li>
                                )}
                                {eatDetail.price && eatDetail.price.message && (
                                  <li>Price: {eatDetail.price.message}</li>
                                )}
                                <li>Rating: {eatDetail.rating}</li>
                              </ul>
                              {eatDetail.website && (
                                <Link href={eatDetail.website}>
                                  {eatDetail.website}
                                </Link>
                              )}
                            </Typography>
                            <img
                              src={eatDetail.photoUrl}
                              alt="resto"
                              className="photoDetail"
                            />
                          </>
                        )}
                      </ListItem>
                      <ListItem button>
                        <IconButton
                          aria-label="add to favorites"
                          onClick={async () => {
                            updateActivity({
                              type: "eat",
                              place: data.name,
                              id: data.id,
                              location: data.location,
                            });
                            setStateModal(false);
                            handleShowMenu(false);
                          }}
                        >
                          <Icon color="primary">add_circle</Icon>
                        </IconButton>
                        <ListItemText primary="Add to trip" />
                      </ListItem>
                    </List>
                  </Collapse>
                </div>
              );
            })}
          </List>
        </Grid>
      </DialogContent>
      <DialogActions className="activityPopUp-buttonClose">
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setStateModal(false);
            handleShowMenu(false);
          }}
        >
          Close
        </Button>
      </DialogActions>
    </>
  );
};

export default EatModal;
