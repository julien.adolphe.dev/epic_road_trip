export interface IActivityModal {
    setStateModal: React.Dispatch<React.SetStateAction<boolean>>
    handleShowMenu: (bool: boolean) => void;
}