import { useState, useContext, useEffect } from "react";
import {
  IconButton,
  Icon,
  List,
  ListItem,
  ListItemText,
  Grid,
  Collapse,
  DialogContent,
  DialogActions,
  Button,
  Typography,
} from "@material-ui/core";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import "./activityModal.scss";
import axios from "axios";
import { RoadtripContext } from "../../context/RoadtripContext";
import { TActivity, TStep } from "../../App";
import { IActivityModal } from "./IActivityModal";
import EnjoyRecommandationCard from "./RecommandationsCard/EnjoyRecommandationCard";
import { findGeoCoords } from "../Maps/utils/maps.utils";

const SleepModal: React.FC<IActivityModal> = ({
  setStateModal,
  handleShowMenu,
}) => {
  const [open, setOpen] = useState<number>();
  const [enjoyData, setEnjoyData] = useState<any[]>([]);
  const [recommandation, setRecommandation] = useState<
    { [key: string]: string }[]
  >([]);
  const { step, stepArray, indexStep, handleUpdateStepArray } =
    useContext(RoadtripContext);
  const [cityCoords, setCityCoords] = useState<{ [key: string]: string }>();

  const findCoordsFromAdress = async () => {
    let plop = await findGeoCoords(stepArray[indexStep].city);
    setCityCoords(plop);
  };

  // Trigger findGeoCoords when step.city
  useEffect(() => {
    findCoordsFromAdress();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step.city]);

  useEffect(() => {
    // fetch all sleep datas
    const fetchData = async () => {
      if (cityCoords !== null && cityCoords?.lat && cityCoords?.lng) {
        await axios
          .get(
            `http://localhost:8001/api/v1/enjoy/activities?latitude=${cityCoords?.lat}&longitude=${cityCoords?.lng}&radius=20`
          )
          .then((result) => {
            setEnjoyData(result.data);
          });
      }
    };

    fetchData();
  }, [cityCoords]);

  useEffect(() => {
    if (enjoyData.length > 0) {
      let temp: any[] = [];
      //get 3 id of the list
      for (let i = 0; i < 3; i++) {
        temp.push(enjoyData[i]);
      }
      setRecommandation(temp);
    }
  }, [enjoyData]);

  const handleClick = (key: number) => {
    setOpen(key);
  };

  //Convert activity object to fit into the model (location)
  function convertActivityObject(
    val: TActivity,
    latitude: Number,
    longitude: Number
  ) {
    let locationObject: { [key: string]: any } = {
      formattedAddress: [
        val.location?.lines,
        val.location?.postalCode,
        val.location?.cityName,
      ],
      lat: latitude,
      lng: longitude,
    };
    val.location = locationObject;
    return val;
  }

  //Update the activity array
  function updateActivity(val: TActivity) {
    let stepToModified: TStep = stepArray[indexStep];
    if (stepToModified.activity) {
      let activityArray: TActivity[] = stepToModified.activity;
      activityArray.push(val);
      stepToModified = { ...stepToModified, activity: activityArray };
    } else {
      stepToModified = { ...stepToModified, activity: [val] };
    }
    let newStepArray = stepArray;
    newStepArray[indexStep] = stepToModified;
    handleUpdateStepArray(newStepArray);
  }
  return (
    <>
      <DialogContent className="activityPopUp">
        {
          <EnjoyRecommandationCard
            recommandation={recommandation}
            updateActivity={updateActivity}
            convertActivityObject={convertActivityObject}
            type="enjoy"
            handleShowMenu={handleShowMenu}
            setStateModal={setStateModal}
          />
        }

        <Grid item lg={8} md={8} sm={8} className="activityPopUp-drinkList">
          <List>
            {enjoyData.map((data, i) => {
              return (
                <div key={i}>
                  <ListItem
                    divider
                    button
                    onClick={() => {
                      handleClick(i);
                    }}
                  >
                    <ListItemText primary={data.name} />
                    {open ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={open === i} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      <ListItem className="activityPopUp-detail">
                        <>
                          <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p"
                          >
                            <ul>
                              {data.price && (
                                <li>
                                  Price: {data.price.amount}{" "}
                                  {data.price.currencyCode}
                                </li>
                              )}
                              <li>Rating: {data.rating}</li>
                              <li>Description: {data.shortDescription}</li>
                            </ul>
                          </Typography>
                          {data.pictures && (
                            <img
                              src={data.pictures[0]}
                              alt="Activity"
                              className="photoDetail"
                            />
                          )}
                        </>
                      </ListItem>
                      <ListItem button>
                        <IconButton
                          aria-label="add to favorites"
                          onClick={async () => {
                            updateActivity(
                              convertActivityObject(
                                {
                                  type: "enjoy",
                                  place: data.name,
                                  id: data.id,
                                  location: data.geoCode,
                                },
                                data.geoCode.latitude,
                                data.geoCode.longitude
                              )
                            );
                            setStateModal(false);
                            handleShowMenu(false);
                          }}
                        >
                          <Icon color="primary">add_circle</Icon>
                        </IconButton>
                          <ListItemText primary="Add to trip" />
                      </ListItem>
                    </List>
                  </Collapse>
                </div>
              );
            })}
          </List>
        </Grid>
      </DialogContent>
      <DialogActions className="activityPopUp-buttonClose">
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setStateModal(false);
            handleShowMenu(false);
          }}
        >
          Close
        </Button>
      </DialogActions>
    </>
  );
};

export default SleepModal;
