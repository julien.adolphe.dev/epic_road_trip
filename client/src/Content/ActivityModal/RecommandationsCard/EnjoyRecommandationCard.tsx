import * as React from "react";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  CardActions,
  IconButton,
  Icon,
  ListItemText,
  ListItem,
} from "@material-ui/core";
import "../activityModal.scss";

import { IAmadeusRecommandationCard } from "./IAmadeusRecommandationCard";

const EnjoyRecommandationCard: React.FC<IAmadeusRecommandationCard> = ({
  setStateModal,
  recommandation,
  updateActivity,
  convertActivityObject,
  type,
  handleShowMenu,
}) => {
  return (
    <>
      {recommandation &&
        recommandation.map((reco: { [key: string]: any }) => {
          console.log(`reco`, reco);
          return (
            <Card className="activityPopUp-card" key={reco.id}>
              <CardHeader
                title={reco.name}
                className="activityPopUp-headerReco"
              />

              {reco.pictures && (
                <img
                  src={reco.pictures[0]}
                  alt="Activity"
                  className="activityPopUp-photoReco"
                />
              )}
              <CardContent>
                {reco.price && (
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Price : {reco.price.amount} {reco.price.currencyCode}
                  </Typography>
                )}

                {reco.rating && (
                  <Typography variant="body2" color="secondary" component="p">
                    Rating : {reco.rating}
                  </Typography>
                )}
              </CardContent>
              <CardActions disableSpacing>
                <ListItem button>
                  <IconButton
                    aria-label="add to favorites"
                    onClick={async () => {
                      updateActivity(
                        convertActivityObject(
                          {
                            type: type,
                            place: reco.name,
                            id: reco.id,
                            location: reco.geoCode,
                          },
                          reco.geoCode.latitude,
                          reco.geoCode.longitude
                        )
                      );
                      setStateModal(false);
                      handleShowMenu(false);
                    }}
                  >
                    <Icon color="primary">add_circle</Icon>
                  </IconButton>
                  <ListItemText primary="Add to trip" />
                </ListItem>
              </CardActions>
            </Card>
          );
        })}
    </>
  );
};

export default EnjoyRecommandationCard;
