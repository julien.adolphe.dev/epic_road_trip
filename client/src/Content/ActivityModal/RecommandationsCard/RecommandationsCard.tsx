import * as React from "react";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  CardActions,
  IconButton,
  Icon,
  Link,
  ListItemText,
} from "@material-ui/core";
import "../activityModal.scss";

import { IRecommandationsCard } from "./IRecommandationsCard";

const RecommandationsCard: React.FC<IRecommandationsCard> = ({
  recommandation,
  updateActivity,
  type,
  handleShowMenu,
  setStateModal
}) => {
  return (
    <>
      {recommandation &&
        recommandation.map((reco: { [key: string]: any }) => {
          return (
            <Card className="activityPopUp-card" key={reco.id}>
              <CardHeader
                title={reco.name}
                className="activityPopUp-headerReco"
              />

              {reco.photoUrl && (
                <img
                  src={reco.photoUrl}
                  alt="resto"
                  className="activityPopUp-photoReco"
                />
              )}
              <CardContent>
                {reco.location && reco.location.formattedAddress &&
                  reco.location.formattedAddress.map(
                    (address: any, i: number) => {
                      return (
                        <Typography
                          key={i}
                          variant="body2"
                          color="textSecondary"
                          component="p"
                        >
                          {address}
                        </Typography>
                      );
                    }
                  )}
                <Typography variant="body2" color="secondary" component="p">
                  Category : {reco.categoryName}
                </Typography>
                {reco.phoneNumber && (
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Phone number : {reco.phoneNumber}
                  </Typography>
                )}
                {reco.price && reco.price.message && (
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Price : {reco.price.message}
                  </Typography>
                )}

                <Typography variant="body2" color="secondary" component="p">
                  Rating : {reco.rating}
                </Typography>
                {reco.website && (
                  <Link href={reco.website} target="blank">
                    {reco.website}
                  </Link>
                )}
              </CardContent>
              <CardActions disableSpacing>
                <IconButton
                  aria-label="add to favorites"
                  onClick={async () => {
                    updateActivity({
                      type: type,
                      place: reco.name,
                      id: reco.id,
                      location: reco.location,
                    });
                    setStateModal(false);
                    handleShowMenu(false);
                  }}
                >
                  <Icon color="primary">add_circle</Icon>
                </IconButton>
                  <ListItemText primary="Add to trip" />
              </CardActions>
            </Card>
          );
        })}
    </>
  );
};

export default RecommandationsCard;
