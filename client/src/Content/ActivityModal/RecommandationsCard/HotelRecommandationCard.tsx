import * as React from "react";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  CardActions,
  IconButton,
  Icon,
  ListItemText,
} from "@material-ui/core";
import "../activityModal.scss";

import { IAmadeusRecommandationCard } from "./IAmadeusRecommandationCard";

const HotelRecommandationCard: React.FC<IAmadeusRecommandationCard> = ({
  setStateModal,
  handleShowMenu,
  recommandation,
  updateActivity,
  convertActivityObject,
  type
}) => {
  return (
    <>
      {recommandation &&
        recommandation.map((reco: { [key: string]: any }, index:number) => {
          return (
            <Card className="activityPopUp-card" key={index}>
              <CardHeader
                title={reco.hotel.name}
                className="activityPopUp-headerReco"
              />
              {reco.hotel.media && (
                <img
                  src={reco.hotel.media[0].uri}
                  alt="Hotel"
                  className="activityPopUp-photoReco"
                />
              )}
              <CardContent>
                {reco.hotel.address &&
                  reco.hotel.address.lines.map(
                    (address: any, i: number) => {
                      return (
                        <Typography
                          key={i}
                          variant="body2"
                          color="textSecondary"
                          component="p"
                        >
                          {address}
                        </Typography>
                      );
                    }
                  )}
                <Typography variant="body2" color="secondary" component="p">
                  Category : {reco.hotel.type}
                </Typography>
                {reco.hotel.contact && reco.hotel.contact.phone && (
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Phone number : {reco.hotel.contact.phone}
                  </Typography>
                )}
                {reco.offers && reco.offers.length > 0 && (
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    Price : {reco.offers[0].price.total} {reco.offers[0].price.currency}
                  </Typography>
                )}

                {reco.hotel.rating && (
                  <Typography variant="body2" color="secondary" component="p">
                  Rating : {reco.hotel.rating}
                  </Typography>
                )}
              </CardContent>
              <CardActions disableSpacing>
                <IconButton
                  aria-label="add to favorites"
                  onClick={async () => {
                    updateActivity( convertActivityObject({
                      type: type,
                      place: reco.hotel.name,
                      id: reco.hotel.hotelId,
                      location: reco.hotel.address,
                    }, reco.hotel.latitude, reco.hotel.longitude));
                    setStateModal(false);
                      handleShowMenu(false);
                  }}
                >
                  <Icon color="primary">add_circle</Icon>
                </IconButton>
                  <ListItemText primary="Add to trip" />
              </CardActions>
            </Card>
          );
        })}
    </>
  );
};

export default HotelRecommandationCard;