import { TActivity } from "../../../App";

export interface IAmadeusRecommandationCard {
  recommandation: { [key: string]: string }[];
  updateActivity: { (val: TActivity): void };
  convertActivityObject: {
    (val: TActivity, latitude: Number, longitude: Number): TActivity;
  };
  type: string;
  handleShowMenu: (bool: boolean) => void;
  setStateModal: React.Dispatch<React.SetStateAction<boolean>>

}
