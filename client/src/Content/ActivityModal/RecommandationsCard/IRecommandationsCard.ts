import { TActivity } from "../../../App";

export interface IRecommandationsCard {
  recommandation: { [key: string]: string }[];
  updateActivity: { (val: TActivity): void };
  type: string;
  handleShowMenu: (bool: boolean) => void;
  setStateModal: React.Dispatch<React.SetStateAction<boolean>>
}
