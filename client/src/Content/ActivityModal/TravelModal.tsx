import { useState } from "react";
import {
  IconButton,
  Icon,
  TextField,
  List,
  ListItem,
  ListItemText,
  Grid,
  Collapse,
  DialogContent,
  DialogActions,
  Button,
  Typography,
} from "@material-ui/core";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import "./activityModal.scss";
import axios from "axios";
import { IActivityModal } from "./IActivityModal";

const TravelModal: React.FC<IActivityModal> = ({
  setStateModal,
  handleShowMenu,
}) => {
  const [open, setOpen] = useState<number>();
  const [travelData, setTravelData] = useState<any[]>([]);
  
  var date = new Date();
  var formatedDate = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`

  const [searchData, setSearchData] = useState({
    originLocationCode: "",
    destinationLocationCode: "",
    departureDate: formatedDate,
    adults: 1
  });

  const fetchData = async () => {
    await axios
      .get(
        `http://localhost:8001/api/v1/travel/flights?originLocationCode=${searchData.originLocationCode}&destinationLocationCode=${searchData.destinationLocationCode}&departureDate=${searchData.departureDate}&adults=${searchData.adults}`
      )
      .then((result) => {
      setTravelData(result.data);
    });
  };

  const handleClick = (key: number) => {
    setOpen(key);
  };

  return (
    <>
      <DialogContent className="activityPopUp">
        <Grid item lg={8} md={8} sm={8} className="activityPopUp-drinkList">
          <Grid item>
            <Grid container direction="row" alignContent="center">
              <TextField
                className="dialog-item-fieldConnection"
                variant="outlined"
                id="standard-basic"
                label="Origin Location Code"
                type="text"
                value={searchData.originLocationCode}
                onChange={(e) =>
                  setSearchData({ ...searchData, originLocationCode: e.target.value })
                }
              />
              <TextField
                className="dialog-item-fieldConnection"
                variant="outlined"
                id="standard-basic"
                label="Destination Location Code"
                type="text"
                value={searchData.destinationLocationCode}
                onChange={(e) =>
                  setSearchData({ ...searchData, destinationLocationCode: e.target.value })
                }
              />
              <TextField
                className="dialog-item-fieldConnection"
                variant="outlined"
                id="standard-basic"
                label="Adults Number"
                type="number"
                value={searchData.adults}
                onChange={(e) =>
                  setSearchData({ ...searchData, adults: parseInt(e.target.value) })
                }
              />
              <TextField
                className="dialog-item-fieldConnection"
                variant="outlined"
                id="standard-basic"
                label="Departure Date"
                type="date"
                onChange={(e) => {
                  setSearchData({ ...searchData, departureDate: e.target.value })
                }
                }
              />
            </Grid>
          </Grid>
          <Grid item>
            <Grid container justify="center" className="dialog-item-buttonsConnection">
              <Button
                variant="contained"
                color="primary"
                className="button"
                onClick={() => {
                  fetchData();
                }}
              >
                Search
              </Button>
            </Grid>
          </Grid>
        </Grid>

        <Grid item lg={8} md={8} sm={8} className="activityPopUp-drinkList">
          <List>
            {travelData.map((data, i) => {
              return (
                <div key={i}>
                  <ListItem
                    divider
                    button
                    onClick={() => {
                      handleClick(i);
                    }}
                  >
                    <ListItemText primary={data.itineraries[0].duration} />
                    {open ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={open === i} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      <ListItem className="activityPopUp-detail">
                        <>
                          <ul>
                            {data.itineraries[0].segments.map(
                              (flight: any, i: number) => {
                                return (
                                  <>
                                    <Typography
                                      key={i}
                                      variant="body2"
                                      color="textSecondary"
                                      component="p"
                                    >
                                      <li>Segment {i}</li>
                                      <li>Départ : {flight.departure.iataCode} to {flight.arrival.iataCode}</li>
                                      <li>Date : {flight.departure.at} - {flight.arrival.at}</li>
                                      <li>Travel Time : {flight.duration}</li>
                                      <li> Carried Code : {flight.carrierCode} {flight.number}</li>
                                    </Typography>
                                  </>
                                );
                              }
                            )}
                          </ul>
                        </>
                      </ListItem>
                      <ListItem button>
                        <IconButton
                          aria-label="add to favorites"
                          onClick={async () => {
                            console.log("Save Flight")
                            setStateModal(false);
                            handleShowMenu(false);
                          }}
                        >
                          <Icon color="primary">add_circle</Icon>
                          <ListItemText primary="Add to trip" />
                        </IconButton>
                      </ListItem>
                    </List>
                  </Collapse>
                </div>
              );
            })}
          </List>
        </Grid>
      </DialogContent>
      <DialogActions className="activityPopUp-buttonClose">
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setStateModal(false);
            handleShowMenu(false);
          }}
        >
          Close
        </Button>
      </DialogActions>
    </>
  );
};

export default TravelModal;