import { useContext, useState } from "react";
import ActivityMenu from "./ActivityMenu/ActivityMenu";
import { IContent } from "./IContent";
import { Grid, Button } from "@material-ui/core";
import "./content.scss";
import SideBar from "./SideBar/SideBar";
import Maps from "./Maps/Maps";
import Login from "./Connection/Login";
import Register from "./Connection/Register";
import { RoadtripContext } from "../context/RoadtripContext";
import MyTrips from "./MyTrips/MyTrips";
import CardTravelIcon from "@material-ui/icons/CardTravel";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const Content: React.FC<IContent> = () => {
  const [openLogin, setOpenLogin] = useState<boolean>(false);
  const [openRegister, setOpenRegister] = useState<boolean>(false);
  const [openTrips, setOpenTrips] = useState<boolean>(false);

  const [reload, setReload] = useState<boolean>(false);
  const [showMenu, setShowMenu] = useState(false);
  function handleShowMenu(bool: boolean) {
    setShowMenu(bool);
  }
  const { userData } = useContext(RoadtripContext);

  const user_id = localStorage.getItem("idUser");
  return (
    <Grid className="content">
      <SideBar showMenu={showMenu} onShowMenu={handleShowMenu} />
      <Grid container justify="flex-end">
        <Grid item lg={4} md={5}>
          {!reload && user_id ? (
            <>
              <Button
                className="content-buttonRegister"
                color="primary"
                variant="contained"
                onClick={() => setOpenTrips(true)}
                startIcon={<CardTravelIcon />}
              >
                My trips
              </Button>
              <Button
                className="content-buttonRegister"
                color="secondary"
                variant="outlined"
                onClick={() => {
                  localStorage.removeItem("idUser");
                  delete userData?.userId;
                  setReload(true);
                }}
                startIcon={<ExitToAppIcon />}
              >
                Logout
              </Button>
            </>
          ) : (
            <>
              <Button
                className="content-buttonRegister"
                color="primary"
                variant="outlined"
                onClick={() => {
                  setOpenRegister(true);
                  setReload(false);
                }}
              >
                Sign up
              </Button>
              <Button
                className="content-buttonRegister"
                color="primary"
                variant="contained"
                onClick={() => {
                  setOpenLogin(true);
                  setReload(false);
                }}
              >
                Login
              </Button>
            </>
          )}
        </Grid>
        {showMenu && (
          <Grid container spacing={2} justify="center">
            <Grid item className="activityMenu">
              <ActivityMenu handleShowMenu={handleShowMenu} />
            </Grid>
          </Grid>
        )}
        <Maps />
      </Grid>
      {openLogin && <Login open={openLogin} setOpenLogin={setOpenLogin} />}
      {openRegister && (
        <Register open={openRegister} setOpenRegister={setOpenRegister} />
      )}
      {openTrips && <MyTrips open={openTrips} setOpenTrips={setOpenTrips} />}
    </Grid>
  );
};

export default Content;
