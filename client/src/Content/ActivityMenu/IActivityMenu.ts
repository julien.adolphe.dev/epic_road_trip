export interface IActivityMenu {
  handleShowMenu: (bool: boolean) => void;
}
