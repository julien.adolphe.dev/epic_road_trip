import * as React from "react";
import { Tabs, Tab, Dialog, DialogTitle } from "@material-ui/core";
import { History } from "history";
import { useHistory, useParams } from "react-router-dom";
import "./activityMenu.scss";
import { IActivityMenu } from "./IActivityMenu";
import DrinkModal from "../ActivityModal/DrinkModal";
import SleepModal from "../ActivityModal/SleepModal";
import EnjoyModal from "../ActivityModal/EnjoyModal";
import EatModal from "../ActivityModal/EatModal";
import TravelModal from "../ActivityModal/TravelModal";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import HotelIcon from "@material-ui/icons/Hotel";
import LocalBarIcon from "@material-ui/icons/LocalBar";
import AirplanemodeActiveIcon from "@material-ui/icons/AirplanemodeActive";
import MoodIcon from "@material-ui/icons/Mood";

const ActivityMenu: React.FC<IActivityMenu> = ({handleShowMenu}) => {
  const history: History = useHistory();

  function handleChange(event: React.ChangeEvent<{}>, tab: number): void {
    history.push({
      pathname: `/activity/${tab}`,
    });
  }
  const { tab } = useParams<{ tab: string }>();
  const tabValue: number = parseInt(tab, 10) || 0;
  const [stateModal, setStateModal] = React.useState(false);

  return (
    <>
      <Tabs
        value={tabValue}
        onChange={handleChange}
        className="tabs"
        textColor="primary"
        indicatorColor="secondary"
      >
        <Tab
          icon={<HotelIcon />}
          label="Sleep"
          onClick={() => {
            setStateModal(true);
          }}
        />
        <Tab
          icon={<FastfoodIcon />}
          label="Eat"
          onClick={() => setStateModal(true)}
        />
        <Tab
          icon={<LocalBarIcon />}
          label="Drink"
          onClick={() => setStateModal(true)}
        />
        <Tab
          icon={<MoodIcon />}
          label="Enjoy"
          onClick={() => setStateModal(true)}
        />
        <Tab
          icon={<AirplanemodeActiveIcon />}
          label="Travel"
          onClick={() => setStateModal(true)}
        />
      </Tabs>
      {tabValue === 0 && (
        <Dialog
          key={tabValue}
          onClose={() => setStateModal(false)}
          open={stateModal}
          fullWidth
          maxWidth={"md"}
        >
          <DialogTitle>
            <h1 className="activityTitle"> Sleep </h1>
          </DialogTitle>
          <SleepModal setStateModal={setStateModal} handleShowMenu={handleShowMenu}/>
        </Dialog>
      )}
      {tabValue === 1 && (
        <Dialog
          key={tabValue}
          onClose={() => setStateModal(false)}
          open={stateModal}
          fullWidth
          maxWidth={"md"}
        >
          <h1 className="activityTitle"> Eat </h1>

          <EatModal setStateModal={setStateModal} handleShowMenu={handleShowMenu}/>
        </Dialog>
      )}
      {tabValue === 2 && (
        <Dialog
          key={tabValue}
          onClose={() => setStateModal(false)}
          open={stateModal}
          fullWidth
          maxWidth={"md"}
        >
          <DialogTitle>
            <h1 className="activityTitle"> Drink </h1>
          </DialogTitle>

          <DrinkModal  setStateModal={setStateModal} handleShowMenu={handleShowMenu}/>
        </Dialog>
      )}
      {tabValue === 3 && (
        <Dialog
          key={tabValue}
          onClose={() => setStateModal(false)}
          open={stateModal}
          fullWidth
          maxWidth={"md"}
        >
          <h1 className="activityTitle"> Enjoy </h1>

          <EnjoyModal  setStateModal={setStateModal} handleShowMenu={handleShowMenu}/>
        </Dialog>
      )}
      { tabValue === 4 && (
        <Dialog
          key={tabValue}
          onClose={() => setStateModal(false)}
          open={stateModal}
          fullWidth
          maxWidth={"md"}
        >
          <h1 className="activityTitle"> Travel </h1>

          <TravelModal  setStateModal={setStateModal} handleShowMenu={handleShowMenu}/>
        </Dialog>
      )}
    </>
  );
};

export default ActivityMenu;
