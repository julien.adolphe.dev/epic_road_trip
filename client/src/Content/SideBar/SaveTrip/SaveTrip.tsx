import React, { useState, useEffect } from "react";
import { Grid, TextField, Button, Typography } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import CheckIcon from "@material-ui/icons/Check";
import SaveIcon from "@material-ui/icons/Save";
import "../sideBar.scss";
import { RoadtripContext } from "../../../context/RoadtripContext";
import { TUserData } from "../../../App";
import { ISaveTrip } from "./ISaveTrip";
import Login from "../../Connection/Login";
import Register from "../../Connection/Register";
import ConfirmationTrip from "../components/ConfirmationTrip/ConfirmationTrip";

const SaveTrip: React.FC<ISaveTrip> = ({
  setSaveTripCheck,
  insertData,
  saveTripCheck,
  confirmationTrip,
}) => {
  const [userTripData, setUserTripData] = useState<TUserData>({
    name: "",
    description: "",
  });
  const [openRegister, setOpenRegister] = useState<boolean>(false);
  const [openLogin, setOpenLogin] = useState<boolean>(false);

  const { userData, setUserData, stepArray } =
    React.useContext(RoadtripContext);

  const updateContextUser = () => {
    let temp = {
      ...userData,
      data: stepArray,
      description: userTripData.description,
      name: userTripData.name,
    };
    setUserData(temp);
  };
  useEffect(() => {
    updateContextUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (userData?.userId) {
    return (
      <Grid
        container
        spacing={2}
        justify="space-around"
        alignItems="center"
        className="sideBar-container"
      >
        <Grid item className="sideBar-fieldDiv-field">
          <Grid container className="sideBar-fieldDiv">
            <Grid item className="sideBar-fieldDiv-field">
              <TextField
                id="standard"
                label="Enter a name for your trip"
                variant="outlined"
                value={userTripData.name}
                onChange={(e) =>
                  setUserTripData({ ...userTripData, name: e.target.value })
                }
              />
            </Grid>
            <Grid item>
              <Button
                size="small"
                className="sideBar-buttons-item"
                variant="contained"
                color="primary"
                onClick={() => {
                  updateContextUser();
                }}
              >
                <CheckIcon />
              </Button>
            </Grid>
          </Grid>
          <Grid container className="sideBar-fieldDiv">
            <Grid item className="sideBar-fieldDiv-field">
              <TextField
                id="standard"
                label="Enter a description"
                variant="outlined"
                value={userTripData.description}
                onChange={(e) =>
                  setUserTripData({
                    ...userTripData,
                    description: e.target.value,
                  })
                }
              />
            </Grid>
            <Grid item>
              <Button
                size="small"
                className="sideBar-buttons-item"
                variant="contained"
                color="primary"
                onClick={() => {
                  updateContextUser();
                }}
              >
                <CheckIcon />
              </Button>
            </Grid>
          </Grid>
        </Grid>
        {saveTripCheck && (
          <Button
            disabled={!userData || (!userData.name && !userData.description)}
            className="sideBar-add"
            variant="contained"
            color="primary"
            onClick={() => {
              // setLocalData(localData);
              setSaveTripCheck(true);
              insertData();
            }}
            startIcon={<SaveIcon />}
          >
            Save my trip
          </Button>
        )}
        {confirmationTrip && <ConfirmationTrip />}
      </Grid>
    );
  } else {
    return (
      <Grid container justify="center">
        <Typography
          variant="h6"
          color="textSecondary"
          component="p"
          className="sideBar-title"
        >
          Do you have an account ?
        </Typography>
        <Grid item className="sideBar-add">
          <Button
            className="sideBar-add"
            color="primary"
            variant="outlined"
            onClick={() => setOpenRegister(true)}
          >
            Sign up
          </Button>
          <Button
            className="sideBar-add"
            color="primary"
            variant="contained"
            onClick={() => {
              setOpenLogin(true);
            }}
          >
            Login
          </Button>
        </Grid>
        <Grid>
          {openLogin && <Login open={openLogin} setOpenLogin={setOpenLogin} />}
          {openRegister && (
            <Register open={openRegister} setOpenRegister={setOpenRegister} />
          )}
        </Grid>
      </Grid>
    );
  }
};
export default SaveTrip;
