
export interface ISaveTrip {
    setSaveTripCheck: (value: React.SetStateAction<boolean>) => void
    insertData : () => Promise<void>
    saveTripCheck:React.SetStateAction<boolean>
    confirmationTrip: boolean
}