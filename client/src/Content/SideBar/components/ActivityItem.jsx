import React from "react";
import { getIcons } from "../../Maps/utils/maps.utils";
import { Typography } from "@material-ui/core";

export default function ActivityItem({ activity }) {
  return (
    <li key={activity.id}>
    <Typography
    variant="body2"
    color="textSecondary"
    component="p"
  >
      <img src={getIcons(activity.type)} alt="activity" />
      {activity.place}
      </Typography>
    </li>
  );
}
