import { useEffect, useState } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Grid,
  DialogActions,
  Button,
  Card,
  CardHeader,
  CardContent,
  Divider,
  Typography,
} from "@material-ui/core";
import ImportExportIcon from "@material-ui/icons/ImportExport";
import PrintIcon from "@material-ui/icons/Print";
// import ShareIcon from "@material-ui/icons/Share";
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { TUserData } from "../../../../App";
import "./confirmationTrip.scss";
import MapsSummary from "../../../Maps/MapsSummary";

const ConfirmationTrip: React.FC = () => {
  const [stateModal, setStateModal] = useState(true);
  const [trip, setTrip] = useState<TUserData>();

  useEffect(() => {
    const tripId: AxiosRequestConfig | undefined = localStorage.getItem(
      "id_trip"
    ) as AxiosRequestConfig | undefined;
    async function getTrip() {
      await axios
        .get(`http://localhost:8000/api/v1/trip/${tripId}`)
        .then((response: AxiosResponse<any>) => {
          setTrip(response.data);
        })
        .catch((err) => {
          window.alert("Une erreur est arrivée");
        });
    }
    getTrip();
  }, []);

  return (
    <Dialog
      onClose={() => setStateModal(false)}
      open={stateModal}
      fullWidth
      maxWidth={"md"}
      className="confirmationTrip"
    >
      <DialogTitle>
        <Typography
          variant="h3"
          color="textSecondary"
          component="h1"
          className="confirmationTrip-title"
        >
          Your trip has been saved !
        </Typography>
      </DialogTitle>
      <DialogContent className="confirmationTrip-content">
        <Grid container justify="center" direction="column">
          <Grid item>
            <Typography
              variant="h4"
              color="textSecondary"
              component="h2"
              className="confirmationTrip-subtitle"
            >
              Summary of your trip :
            </Typography>
          </Grid>
          <Grid item className="confirmationTrip-content-list">
            <ul>
              <li>Name : {trip?.name}</li>
              <li>Description : {trip?.description}</li>
              {trip?.data &&
                trip.data.map((step, index) => {
                  return (
                    <>
                      <Card key={index}>
                        <CardHeader
                          title={`Step ${index + 1} : ${step.city}`}
                        ></CardHeader>
                        <Divider />
                        <CardContent>
                          {step.activity &&
                            step.activity.map((activityStep) => {
                              return (
                                <>
                                  <li>Type of activity: {activityStep.type}</li>
                                  <li>Place: {activityStep.place}</li>
                                  <Divider />
                                </>
                              );
                            })}
                        </CardContent>
                      </Card>
                    </>
                  );
                })}
            </ul>
          </Grid>
        </Grid>
        <MapsSummary />
      </DialogContent>
      <DialogActions className="confirmationTrip-footer">
        <Grid container justify="center">
          <Button
            variant="contained"
            color="primary"
            className="confirmationTrip-footer-button"
            startIcon={<PrintIcon />}
            onClick={() => {
              window.print();
              setStateModal(false);
              localStorage.removeItem("id_trip");
            }}
          >
            Print my trip
          </Button>
          {/* <Button
            variant="outlined"
            color="primary"
            className="confirmationTrip-footer-button"
            startIcon={<ShareIcon />}
            onClick={() => {
              setStateModal(false);
            }}
          >
            Share my trip
          </Button> */}
          <Button
            variant="outlined"
            color="secondary"
            className="confirmationTrip-footer-button"
            onClick={() => {
              setStateModal(false);
              localStorage.removeItem("id_trip");
            }}
          >
            Close
          </Button>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationTrip;
