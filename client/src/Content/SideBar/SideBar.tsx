import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { TextField, Button, Grid, Typography } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import SaveIcon from "@material-ui/icons/Save";
import CheckIcon from "@material-ui/icons/Check";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import "./sideBar.scss";
import { ISideBar } from "./ISideBar";
import { RoadtripContext } from "../../context/RoadtripContext";
import { TActivity, TStep, TUserData } from "../../App";
import Header from "../../Header/Header";
// import { getIcons } from "../Maps/utils/maps.utils";
import ActivityItem from "./components/ActivityItem";
import SaveTrip from "./SaveTrip/SaveTrip";
import axios from "axios";

const SideBar: React.FC<ISideBar> = ({ showMenu, onShowMenu }) => {
  let history = useHistory();

  const [localData, setLocalData] = useState<TUserData>({});
  const [confirmationTrip, setConfirmationTrip] = useState(false);

  const {
    handleUpdateStepArray,
    stepArray,
    handleUpdateStep,
    step,
    setIndexStep,
    userData,
    handleUpdateUserData,
  } = React.useContext(RoadtripContext);
  //useEffect to update the step object
  useEffect(() => {
    handleUpdateStep(step);
    // handleUpdateUserData(userData);
  }, [handleUpdateStep, handleUpdateUserData, step, userData]);

  //update the step array, which is the global object
  const [localStepArray, setLocalTestStepArray] = useState<TStep[]>([]);

  function updateStepArray(val: TStep) {
    let newStep = localStepArray;
    newStep.push(val);
    setLocalTestStepArray(newStep);
    handleUpdateStepArray(localStepArray);
    setLocalData({ data: localStepArray });
  }

  const insertData = async () => {
    let formattedStepArray: TStep[] = [];
    let formattedStep: TStep = {};
    let formattedActivity: TActivity[][] = [];
    let finalFormattedData: TStep[] = [];
    if (userData?.data) {
      for (let arr = 0; arr < userData.data.length; arr++) {
        let step: TStep = userData.data[arr];
        formattedStepArray.push(step);
        for (let i in formattedStepArray) {
          let tempArray: TActivity[] = [];
          formattedStep = formattedStepArray[i];
          let tempActivity: TActivity[] | undefined =
            formattedStepArray[i].activity;
          if (tempActivity)
            for (let j = 0; j < tempActivity.length; j++) {
              let temp: TActivity = {
                type: tempActivity[j].type,
                place: tempActivity[j].place,
              };
              tempArray.push(temp);
              delete formattedStep.activity;
            }
          formattedStep = { ...formattedStep, activity: tempArray };
          formattedActivity.push(tempArray);
        }
        finalFormattedData.push(formattedStep);
      }
    }

    let data = {
      userId: userData?.userId,
      name: userData?.name,
      description: userData?.description,
      data: finalFormattedData,
    };
    await axios
      .post(`http://localhost:8000/api/v1/trip/save`, data)
      .then((response) => {
        localStorage.setItem("id_trip", response.data.trip.id);
        console.log(`response`, response);
        window.alert("Données insérées avec succès");
        setConfirmationTrip(true);
      })
      .catch((err) => {
        window.alert("Une erreur est arrivée");
      });
  };

  //All next functions to manage the fields and display of buttons
  const [saveTripCheck, setSaveTripCheck] = useState(false);
  const [cityArray, setCityArray] = useState<TStep[]>([{ city: "" }]);

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    index: number
  ) => {
    const { value } = e.target;
    const list: TStep[] = [...cityArray];
    list[index].city = value;
    setCityArray(list);
  };
  const [checked, setChecked] = useState<number[]>([]);
  const addCheck = (index: number): void => {
    const selected = checked;
    selected.push(index);
    setChecked(selected);
  };
  const removeCheck = (index: number) => {
    const selected = checked;
    selected.splice(index);
    setChecked(selected);
  };

  const handleRemoveClick = (index: number) => {
    const list = [...cityArray];
    list.splice(index, 1);
    setCityArray(list);
    let temp = localStepArray;
    temp.splice(index, 1);
    setLocalTestStepArray(temp);
  };

  const handleAddClick = () => {
    setCityArray([...cityArray, { city: "", activity: [] }]);
  };

  useEffect(() => {
    handleUpdateStepArray(localStepArray);
  }, [handleUpdateStepArray, localStepArray]);

  return (
    <>
      <div className="sideBar">
        <Header />
        <Typography
          variant="h2"
          color="textSecondary"
          component="h1"
          className="sideBar-title"
        >
          Plan your roadtrip
        </Typography>
        <div className="sideBar-content">
          <>
            {cityArray.map((data, index) => {
              const isChecked = checked.includes(index);
              return (
                <div key={index} className="test">
                  <Grid
                    container
                    spacing={2}
                    justify="space-around"
                    alignItems="center"
                    className="sideBar-container"
                  >
                    <Grid item>
                      <Grid container className="sideBar-fieldDiv">
                        <Grid item className="sideBar-fieldDiv-field">
                          <TextField
                            disabled={isChecked}
                            key={index}
                            id="standard"
                            label="Step"
                            variant="outlined"
                            value={data.city}
                            onChange={(e) => {
                              handleInputChange(e, index);
                            }}
                          />
                        </Grid>
                        <Grid item>
                          {!isChecked && (
                            <Button
                              size="small"
                              className="sideBar-buttons-item"
                              variant="contained"
                              color="primary"
                              onClick={() => {
                                addCheck(index);
                                let newObj: TStep = {
                                  city: data.city,
                                  idCity: index,
                                };
                                handleUpdateStep(newObj);
                                updateStepArray(newObj);
                              }}
                            >
                              <CheckIcon />
                            </Button>
                          )}
                          {cityArray.length !== 1 && isChecked && (
                            <>
                              <Button
                                size="small"
                                color="secondary"
                                variant="outlined" //outlined
                                className="sideBar-buttons-item"
                                onClick={() => {
                                  removeCheck(index);
                                  handleRemoveClick(index);
                                }}
                              >
                                <RemoveCircleOutlineIcon />
                              </Button>
                            </>
                          )}
                        </Grid>
                      </Grid>
                      {cityArray.length !== 0 && isChecked && (
                        <Grid container justify="flex-end">
                          <Button
                            size="small"
                            variant="contained"
                            color="primary"
                            onClick={() => {
                              onShowMenu(!showMenu);
                              history.push("/");
                              setIndexStep(index);
                            }}
                            startIcon={<AddCircleIcon />}
                          >
                            Add an activity
                          </Button>
                        </Grid>
                      )}
                    </Grid>
                    {stepArray &&
                      stepArray[index] &&
                      stepArray[index].hasOwnProperty("activity") && (
                        <Grid container justify="flex-start">
                          <Grid item>
                            <ul>
                              {stepArray[index].activity?.map((el) => {
                                return <ActivityItem activity={el} />;
                              })}
                            </ul>
                          </Grid>
                        </Grid>
                      )}
                    {cityArray.length - 1 === index &&
                      checked &&
                      !saveTripCheck && (
                        <Grid container justify="center">
                          <Grid item className="sideBar-add">
                            <Button
                              className="sideBar-add"
                              variant="contained"
                              color="primary"
                              onClick={() => {
                                handleAddClick();
                              }}
                              startIcon={<AddCircleIcon />}
                            >
                              Add a step
                            </Button>
                            <Button
                              className="sideBar-add"
                              variant="contained"
                              color="secondary"
                              onClick={() => {
                                setLocalData(localData);
                                setSaveTripCheck(true);
                              }}
                              startIcon={<SaveIcon />}
                            >
                              Save my trip
                            </Button>
                          </Grid>
                        </Grid>
                      )}
                    {cityArray.length - 1 === index &&
                      checked &&
                      saveTripCheck && (
                        <>
                          <SaveTrip
                            setSaveTripCheck={setSaveTripCheck}
                            insertData={insertData}
                            saveTripCheck={saveTripCheck}
                            confirmationTrip={confirmationTrip}
                          />
                        </>
                      )}
                  </Grid>
                </div>
              );
            })}
          </>
        </div>
      </div>
    </>
  );
};

export default SideBar;
