
export interface ISideBar {
  showMenu: boolean
  onShowMenu: (bool: boolean) => void;
}
