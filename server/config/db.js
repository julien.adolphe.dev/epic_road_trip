const mongoose = require('mongoose');
const mongoUri = process.env.MONGO_URI;
mongoose.set('useCreateIndex', true)

//Connect to DB
mongoose
    .connect(mongoUri,{ useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log("Connected correctly to db");
    })
    .catch((err) => {
        console.log("Unsuccessful db connection", err);
    });
