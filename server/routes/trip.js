const router = require('express').Router();
const Trip = require('../models/Trip');
const Step = require('../models/Step');
const Activity = require('../models/Activity');
const TripService = require('../services/tripService');

router.post('/save2', async (req, res) =>
{
    const activity = new Activity({
        type: req.body.type,
        place: req.body.place,
        location: req.body.location,
        description: req.body.description,
        openingHours: req.body.openingHours,
    })
    const step = new Step({
        city: req.body.city,
        description: req.body.description,
        activity: activity,
    })
    const trip = new Trip({
        userId: req.body.userId,
        name: req.body.name,
        description: req.body.description,
        data: step
    });
    try {
        await trip.save();
        res.send({ trip: trip.name });
        console.log({ trip: trip.name }, "saved in the database.");
    }
    catch (err) {
        res.status(400).send(err);
    }
});

router.post('/save', async (req, res, next) =>
{
    const createdTrip = await TripService.saveTrip(req.body).catch(next)
    res.send(createdTrip);
    console.log({ trip: createdTrip.trip.name }, "saved in the database.");
});

router.get('/:tripId', async (req, res, next) =>
{
    const tripRes = await TripService.getTripById(req.params.tripId).catch(next)
    res.json(tripRes);
});

router.get('/user/:userId', async (req, res, next) =>
{
    const tripRes = await TripService.getTripsByUserId(req.params.userId).catch(next)
    res.json(tripRes);
});

router.delete('/delete/:tripId', async (req, res, next) =>
{
    const deletedTrip = await TripService.deleteTripById(req.params.tripId).catch(next)
    if(deletedTrip)
        res.send(`\"${deletedTrip}.name\" trip successfully deleted`);
    else
        res.send("Trip could not be deleted");
});

module.exports = router;