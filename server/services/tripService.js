const Trip = require('../models/Trip');

module.exports = {

    async saveTrip(tripData) {
        const trip = new Trip({
            userId: tripData.userId,
            name: tripData.name,
            description: tripData.description,
            data: tripData.data
        });
        await trip.save();
        return ({trip: { id: trip._id, name: trip.name }});
    },

    async getTripById(tripId) {
        const trip = await Trip.findById({_id: tripId});
        return trip;
    },

    async getTripsByUserId(userId) {
        const userTrips =  await Trip.find({userId: userId});
        return userTrips;
    },

    async deleteTripById(tripId) {
        const deleteTrip =  await Trip.findByIdAndRemove(tripId);
        return deleteTrip;
    }
  
};