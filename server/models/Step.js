const mongoose = require('mongoose');
const Activity = require('../models/Activity').schema;

const Schema = mongoose.Schema;

const stepSchema = new Schema({
    idCity: {
        type: Number,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    activity: [Activity],
    stepDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Step', stepSchema);
