const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const activitySchema = new Schema({
    id: {
        type: String,
        required: false,
    },
    type: {
        type: String,
        required: false
    },
    place: {
        type: String,
        required: true,
    },
    placeId: {
        type: Number,
        required: false,
    },
    location: {
        type: String,
        required: false,
    },
    description: {
        type: String,
        required: false,
    },
    openingHours: {
        type: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Activity', activitySchema);
