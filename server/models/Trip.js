const mongoose = require('mongoose');
const Step = require('../models/Step').schema;

const Schema = mongoose.Schema;

const tripSchema = new Schema({
    userId: {
        type: mongoose.ObjectId,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    data: [Step],
    creationDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Trip', tripSchema);

