const express = require('express');
const cors = require("cors")
const app = express();

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./swagger.yaml');

const dotenv = require('dotenv');
dotenv.config();

const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const port = process.env.PORT || 8000;

//Import routes
const authRoute = require('./routes/auth');
const tripRoute = require('./routes/trip');
const postRoute = require('./routes/posts');

require("./config/db");

const corsOptions = {
    origin: "*"
};

app.use(cors(corsOptions));

//Middleware
app.use(express.json());

// Swagger API docs
app.use('/api-docs', function(req, res, next){
    swaggerDocument.host = req.get('host');
    req.swaggerDoc = swaggerDocument;
    next();
}, swaggerUi.serve, swaggerUi.setup());

app.get("/", (req, res) => {
    res.send("Hello from SERVER MICROSERVICE \n");
});

//Route Middlewares
app.use('/api/v1/user', authRoute);
app.use('/api/v1/trip', tripRoute);
app.use('/api/posts', postRoute);

//Error handling middleware
app.use((err, req, res, next) => {
    console.error(err)
    return res.status(500).json(error);
});

app.listen(port, () => console.log('Server up and running'));