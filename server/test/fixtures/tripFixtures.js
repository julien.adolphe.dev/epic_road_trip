exports.validTripData = {
    "userId": "60b0d7801797e5003d72fc28",
    "creationDate": "2021-07-08T18:25:43.511Z",
    "name": "Vacances Juillet",
    "description": "On pars à ToutouLand",
    "data": [
        {
            "step": 1,
            "address": "Toulouse",
            "stepDate": "2021-07-08T18:25:43.511Z",
            "description": "Wouhou c'est chouette",
            "activities": [
            {
                "id": 1,
                "type": "Drinks",
                "placeId": 3513453415,
                "name": "Le bar Basque",
                "address": "Place St Pierre, TOULOUSE",
                "date": "2021-07-08T18:25:43.511Z",
                "description": "Mmh la bière",
                "openingHours": "10h-00h"
            },
            {
                "id": 2,
                "type": "Eat",
                "placeId": 3513453415,
                "name": "Le Manger Avant Tout",
                "address": "Place Manger, TOULOUSE",
                "date": "2021-07-08T18:25:43.511Z",
                "description": "Mmh le manger",
                "openingHours": "10h-00h"
            }
            ]
        },
        {
            "step": 2,
            "address": "Montpellier",
            "stepDate": "2021-07-08T18:25:43.511Z",
            "description": "Wouhou c'est chouette 2",
            "activities": [
            {
                "id": 1,
                "type": "Drinks",
                "placeId": 3513453415,
                "name": "Le bar Basque2",
                "address": "Place St Pierre, TOULOUSE",
                "date": "2021-07-08T18:25:43.511Z",
                "description": "Mmh la bière 2",
                "openingHours": "10h-00h"
            },
            {
                "id": 2,
                "type": "Eat",
                "placeId": 3513453415,
                "name": "Le Manger Avant Tout 2",
                "address": "Place Manger, TOULOUSE",
                "date": "2021-07-08T18:25:43.511Z",
                "description": "Mmh le manger 2",
                "openingHours": "10h-00h"
            }
            ]
        }
    ]
};