const mongoose = require("mongoose");
const TripService = require('../services/tripService');
const { dbConnect, dbDisconnect } = require('./db');

const { validTripData } = require('./fixtures/tripFixtures');

let createdTripId;

beforeAll(async () => {
    dbConnect()
    const createdTrip = await TripService.saveTrip(validTripData);
    createdTripId = createdTrip.trip.id.toString();
});
afterAll(async () => dbDisconnect());

describe("Get Trip By Id", () => {

  it('Get trip with valid id', async () => {
    const foundTrip = await TripService.getTripById(createdTripId);

    expect(foundTrip._id.toString()).toBe(createdTripId);
    expect(foundTrip.name).toBe(validTripData.name);
    expect(foundTrip.userId.toString()).toBe(validTripData.userId);
    expect(foundTrip.description).toBe(validTripData.description);
    expect(foundTrip.data).toEqual(expect.arrayContaining([]));
  });

  it('Get trip with invalid (non-existent) id', async () => {
    const invalidId = "wrongId";

    await expect(TripService.getTripById(invalidId)).rejects.toThrow();
  });

});

describe("Get Trips By User Id", () => {
  const validUserId = "60b0d7801797e5003d72fc28"

  it('Get trip with valid user id', async () => {
    const foundTrips = await TripService.getTripsByUserId(validUserId);

    expect(foundTrips.length).toBe(1);
    expect(foundTrips[0]._id.toString()).toEqual(createdTripId);
    expect(foundTrips[0].userId.toString()).toEqual(validUserId);
  });

  it('Get trip of a user who has no trips (non-existent user)', async () => {
    const invalidId = "41224d776a326fb40f000001"; //Can't just put anything here, it must have a given format for Mongo ObjectId

    const foundTrips = await TripService.getTripsByUserId(invalidId);

    expect(foundTrips.length).toBe(0);
    //Test if it is an array
    expect(foundTrips).toEqual(expect.arrayContaining([]));
  });

});
