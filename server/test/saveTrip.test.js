const mongoose = require("mongoose");
const Trip = require('../models/Trip');
const TripService = require('../services/tripService');
const { dbConnect, dbClear, dbDisconnect } = require('./db');

const { validTripData } = require('./fixtures/tripFixtures');

beforeAll(async () => dbConnect());
afterEach(async () => dbClear());
afterAll(async () => dbDisconnect());


describe("Create Trip Test", () => {

  it('Save valid trip', async () => {
    const createdTrip = await TripService.saveTrip(validTripData);
    // Object Id should be defined when successfully saved to MongoDB.
    createdTripId = createdTrip.trip.id;
    expect(createdTrip.trip.id).toBeDefined();
    expect(createdTrip.trip.name).toBe(validTripData.name);
  });

  // Test Schema is working!!!
  // You shouldn't be able to add in any field that isn't defined in the schema
  //Commented because saveTrip only returns id and name, so additional fields would always be undefined
  /*it('Save trip with additional field not defined in schema (field should be undefined)', async () => {
      const tripWithInvalidField = validTripData;
      tripWithInvalidField.invalidField = "invalidField";
      const savedTripWithInvalidField = await TripService.saveTrip(tripWithInvalidField);
      expect(savedTripWithInvalidField.trip.id).toBeDefined();
      expect(savedTripWithInvalidField.trip.invalidField).toBeUndefined();
  });*/

  // Test Validation is working!!!
  // It should tell us the errors in on date field.
  it('Save trip with missing field', async () => {
    const tripWithoutRequiredField = validTripData;
    delete tripWithoutRequiredField.name;

    await expect(TripService.saveTrip(tripWithoutRequiredField)).rejects.toThrow();
  });

});
