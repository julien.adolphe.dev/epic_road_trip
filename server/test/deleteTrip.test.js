const mongoose = require("mongoose");
const TripService = require('../services/tripService');
const { dbConnect, dbDisconnect } = require('./db');

const { validTripData } = require('./fixtures/tripFixtures');

let createdTripId;

beforeAll(async () => {
    dbConnect()
    const createdTrip = await TripService.saveTrip(validTripData);
    createdTripId = createdTrip.trip.id.toString();
});

afterAll(async () => dbDisconnect());

describe("Delete Trip By Id", () => {

  it('Delete trip with valid id should return removed object', async () => {
    const deletedTrip = await TripService.deleteTripById(createdTripId);

    expect(deletedTrip._id.toString()).toBe(createdTripId);

    const foundTrip = TripService.getTripById(createdTripId);
    expect(Object.keys(foundTrip).length).toBe(0);
  });

  it('Delete non-existent trip should return null', async () => {
    const invalidId = "41224d776a326fb40f000001"; //Can't just put anything here, it must have a given format for Mongo ObjectId

    const deletedTrip = await TripService.deleteTripById(invalidId);

    expect(deletedTrip).toBeNull();
    expect(deletedTrip).toBeFalsy();
  });

});