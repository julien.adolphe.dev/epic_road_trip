const router = require('express').Router();

const drink = require('../controllers/drink_controller')

router.get('/bars/:location', drink.getBars);

router.get('/bar/:id', drink.getBarDetails);

module.exports = router;
