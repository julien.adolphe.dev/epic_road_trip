const router = require('express').Router();

const eat = require('../controllers/eat_controller')  

router.get('/restaurants/:location', eat.getRestaurants);

router.get('/restaurant/:id', eat.getRestaurantDetails);

module.exports = router;