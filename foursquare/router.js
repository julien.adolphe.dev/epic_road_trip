const router = require('express').Router();

const controller = require('./controller')  

router.get('/eat/restaurants/:location', (req,res) => { controller.getPlaces(req,res,"eat") });

router.get('/eat/restaurant/:id', controller.getPlaceDetails);

router.get('/drink/bars/:location', (req,res) => { controller.getPlaces(req,res,"drink") });

router.get('/drink/bar/:id', controller.getPlaceDetails);

module.exports = router;