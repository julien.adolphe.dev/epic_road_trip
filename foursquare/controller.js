const axios = require("axios");

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const EAT_SECTION = "food";
const DRINK_SECTION = "drinks";

const foursquare = require('node-foursquare-venues')(CLIENT_ID, CLIENT_SECRET)

async function placesByLocationCall(location, category, options = {}, callback) {
    requestParams = {
        near: location,
        day: "any",
        time: "any"
    };

    if(options.hasOwnProperty("price")) requestParams.price = options.price;

    if(options.hasOwnProperty("type")) requestParams.query = options.type;
    else if(category === "eat") requestParams.section = EAT_SECTION;
    else requestParams.section = DRINK_SECTION;

    await foursquare.venues.explore(requestParams, callback);
};

async function placeDetailsById(id, callback) {
    await foursquare.venues.venue(id, {}, callback);
};

function filterPlaceProperties(restaurant){
    const {id, name, location} = restaurant;
    resObject = {id, name, location};
    return resObject;
}

function filterPlaceDetailsProperties(restaurant){
    const {
        id, 
        name, 
        contact: {formattedPhone: phoneNumber}, 
        location, 
        categories: [{shortName: categoryName}],
        url: website,
        price,
        likes: {count: likeNb},
        rating,
        photos,
        hours,
        popular: popularHours,
        bestPhoto,
    } = restaurant;

    photoUrl = "";
    if(bestPhoto){
        photoUrl = bestPhoto.prefix + bestPhoto.width + "x" + bestPhoto.height + bestPhoto.suffix;
    } else if (photos.count > 0) {
        photoItem = photos.groups[0].items[0];
        photoUrl = photoItem.prefix + photoItem.width + "x" + photoItem.height + photoItem.suffix;
    }

    //If hours is undefined but popular hours is not assign popularHours to hours
    realHours = hours;
    if(!hours && popularHours){ 
        realHours = popularHours;
    }

    resObject = {id, name, phoneNumber, location, categoryName, website, price, likeNb, rating, photoUrl, realHours};

    return resObject;
}

module.exports = {

    async getPlaces(req, res, category) {
        await placesByLocationCall(req.params.location, category, req.query, 
            function(error, response) {
                if (error) { res.json(error); return console.error(error) }
    
                const groups = response.response.groups[0].items;
                const restaurants = [];
                groups.forEach(element => {
                    const newElement = filterPlaceProperties(element.venue)
                    restaurants.push(newElement)
                });
                res.json(restaurants);
            }
        );
    },

    async getPlaceDetails(req, res) {
        await placeDetailsById(req.params.id,
            function(error, response) {
                if (error) { res.json(error); return console.error(error) }
    
                restaurant = response.response.venue
                finalRestaurant = filterPlaceDetailsProperties(restaurant)
                res.json(finalRestaurant);
            }
        );
    }
  
};