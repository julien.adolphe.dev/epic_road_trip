const axios = require("axios");

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const SECTION = "food";

const foursquare = require('node-foursquare-venues')(CLIENT_ID, CLIENT_SECRET)

async function restaurantsByLocationCall(location, options = {}, callback) {
    requestParams = {
        near: location,
        day: "any",
        time: "any"
    };

    if(options.hasOwnProperty("price")) requestParams.price = options.price;

    if(options.hasOwnProperty("type")) requestParams.query = options.type;
    else requestParams.section = SECTION;

    await foursquare.venues.explore(requestParams, callback);
};

async function restaurantDetailsById(id, callback) {
    await foursquare.venues.venue(id, {}, callback);
};

function filterRestaurantProperties(restaurant){
    const {id, name, location, bestPhoto} = restaurant;
    resObject = {id, name, location, bestPhoto};
    return resObject;
}

function filterRestaurantDetailsProperties(restaurant){
    const {
        id, 
        name, 
        contact: {formattedPhone: phoneNumber}, 
        location: {formattedAddress: address, lng, lat}, 
        categories: [{shortName: categoryName}],
        url: website,
        price,
        likes: {count: likeNb},
        rating,
        hours,
        popular: popularHours,
        // bestPhoto,
    } = restaurant;

    // if(bestPhoto){
    //     photoUrl = bestPhoto.prefix + bestPhoto.width + "x" + bestPhoto.height + bestPhoto.suffix;
    // }

    if(!hours){
        if(popularHours) hours = popularHours;
    }

    resObject = {id, name, phoneNumber, location:{address, lng, lat}, categoryName, website, price, likeNb, rating, hours}

    return resObject;
}

module.exports = {

    async getRestaurants(req, res) {
        await restaurantsByLocationCall(req.params.location, req.query, 
            function(error, response) {
                if (error) { return console.error(error) }
    
                const groups = response.response.groups[0].items;
                const restaurants = [];
                groups.forEach(element => {
                    const newElement = filterRestaurantProperties(element.venue)
                    restaurants.push(newElement)
                });
                res.json(restaurants);
            }
        );
    },

    async getRestaurantDetails(req,res) {
        await restaurantDetailsById(req.params.id,
            function(error, response) {
                if (error) { return console.error(error) }
    
                restaurant = response.response.venue
                finalRestaurant = filterRestaurantDetailsProperties(restaurant)
                res.json(finalRestaurant);
            }
        );
    }
  
};