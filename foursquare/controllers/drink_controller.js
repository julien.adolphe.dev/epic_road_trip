const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const SECTION = "drinks";

const foursquare = require('node-foursquare-venues')(CLIENT_ID, CLIENT_SECRET)

async function barsByLocationCall(location, options = {}, callback)
{
    const requestParams = {
        near: location,
        day: "any",
        time: "any"
    };
    if(options.hasOwnProperty("price"))
    {
        requestParams.price = options.price
    }
    if(options.hasOwnProperty("type"))
    {
        requestParams.query = options.type
    }
    else
    {
        requestParams.section = SECTION
    }
    await foursquare.venues.explore(requestParams, callback);
};

async function barDetailsById(id, callback)
{
    await foursquare.venues.venue(id, {}, callback);
};

function filterBarProperties(bar){
    const {id, name, location, bestPhoto} = bar;
    const resObject = {id, name, location, bestPhoto};
    return resObject;
}

function filterBarDetailsProperties(bar){
    const {
        id,
        name,
        contact: {formattedPhone: phoneNumber},
        location: {formattedAddress: address, lng, lat},
        categories: [{shortName: categoryName}],
        url: website,
        price,
        likes: {count: likeNb},
        rating,
        hours,
        popular: popularHours,
        bestPhoto,
    } = bar;

    // if(bestPhoto){
    //     let photoUrl = bestPhoto.prefix + bestPhoto.width + "x" + bestPhoto.height + bestPhoto.suffix;
    // }
    if(!hours){
        if(popularHours)
        {
            let hours = popularHours
        };
    }

    const resObject = {id, name, phoneNumber, location:{address, lng, lat}, categoryName, website, price, likeNb, rating, hours}

    return resObject;
}

module.exports = {
    async getBars(req, res) {
        await barsByLocationCall(req.params.location, req.query,
            function(error, response) {
                if (error) { return console.error(error) }

                const groups = response.response.groups[0].items;
                const bars = [];
                groups.forEach(element => {
                    const newElement = filterBarProperties(element.venue)
                    bars.push(newElement)
                });
                res.json(bars);
            }
        );
    },
    async getBarDetails(req,res) {
        await barDetailsById(req.params.id,
            function(error, response) {
                if (error) { return console.error(error) }

                const bar = response.response.venue
                const finalBar = filterBarDetailsProperties(bar)
                res.json(finalBar);
            }
        );
    }
};
