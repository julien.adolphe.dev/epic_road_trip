const express = require("express");
const cors = require("cors")
const PORT = 8002;
const HOST = '0.0.0.0';

const app = express();

const corsOptions = {
    origin: "*"
};

app.use(cors(corsOptions));

const dotenv = require('dotenv');
dotenv.config();

const routes = require('./router');

app.get("/", (req, res) => {
    res.send("Hello from FOURSQUARE MICROSERVICE \n");
});

app.use('/api/v1', routes);

app.all("*", (req, res) => res.send("You've tried reaching a route that doesn't exist."));

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});