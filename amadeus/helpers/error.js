const handleError = (err, res) => {
    const { result, statusCode } = err.response;
    console.log(result.errors)
    res.status(statusCode).json(result.errors[0]);
};
  
module.exports = {
    handleError
}
  