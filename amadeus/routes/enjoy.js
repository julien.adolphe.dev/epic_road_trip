const router = require('express').Router();

const enjoy = require('../controllers/enjoy_controller')

router.get('/activities', enjoy.getActivities)
router.get('/activity/:id', enjoy.getActivityById)

router.get('/points', enjoy.getPointsOfInterest)
router.get('/point/:id', enjoy.getPointOfInterestById)

module.exports = router;
