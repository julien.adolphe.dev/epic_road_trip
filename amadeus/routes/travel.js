const router = require('express').Router();

const travel = require('../controllers/travel_controller')

router.get('/flights', travel.getFlights)

module.exports = router;