const router = require('express').Router();

const sleep = require('../controllers/sleep_controller')

router.get('/hotels', sleep.getHotels)
router.get('/hotel', sleep.getHotelOffers)

module.exports = router;
