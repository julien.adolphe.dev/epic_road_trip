const { ErrorHandler } = require('../helpers/error')

var AmadeusApk = require('amadeus');

const API_KEY = process.env.CLIENT_ID;
const API_SECRET = process.env.CLIENT_SECRET;

var amadeus = new AmadeusApk({
  clientId: API_KEY,
  clientSecret: API_SECRET
});

function filterActivityProperties(activity){
    const {id, name, shortDescription, geoCode, rating, pictures, price} = activity;
    resObject = {id, name, shortDescription, geoCode, rating, pictures, price};
    return resObject;
}

function filterPointProperties(point){
    const {id, name, category, geoCode, tags} = point;
    resObject = {id, name, category, geoCode, tags};
    return resObject;
}

module.exports = {

    async getActivities(req, res, next) {
        const {latitude, longitude, radius, maxPrice} = req.query
        requestParams = {latitude, longitude, radius}
        
        try {
            const {data: activitiesRes} = await amadeus.shopping.activities.get(requestParams)
            
            activities = []
            activitiesRes.forEach(element => {
                newActivity = filterActivityProperties(element);
                activities.push(newActivity);
            });
    
            //Filter by price
            if(maxPrice && maxPrice > 0) {
                activities = activities.filter(element => element.price.amount <= maxPrice)
            }
        
            res.json(activities);
            next();
        } catch(error) {next(error)};
    },

    async getActivityById(req, res, next) {
        const {data: activityRes} = await amadeus.shopping.activity(req.params.id).get()
        .catch((error) => next(error));

        activity = filterActivityProperties(activityRes);
    
        res.json(activity);
        next();
    },

    async getPointsOfInterest(req, res, next) {
        const {data: pointRes} = await amadeus.referenceData.locations.pointsOfInterest.get(req.query)
        .catch((error) => next(error));

        points = []
        pointRes.forEach(element => {
            newPoint = filterPointProperties(element);
            points.push(newPoint);
        });
    
        res.json(points);
        next();
    },

    async getPointOfInterestById(req, res) {
        const {data: pointRes} = await amadeus.referenceData.locations.pointOfInterest(req.params.id).get()
        .catch((error) => next(error));

        point = filterPointProperties(pointRes);
    
        res.json(point);
        next();
    }
};
