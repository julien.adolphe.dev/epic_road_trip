const Amadeus = require('amadeus');

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;

const amadeus = new Amadeus({
    clientId: CLIENT_ID,
    clientSecret: CLIENT_SECRET
});

function filterHotelProps(place)
{
    const { hotel, available, offers } = place;
    const resObj = { hotel, available, offers };
    return resObj;
}

module.exports = {

    async getHotels(req, res) {
        const { cityCode, latitude, longitude, radius, radiusUnit, checkInDate, checkOutDate, priceRange, currency, hotelName, ratings } = req.query;
        const reqParams = { cityCode, latitude, longitude, radius, radiusUnit, checkInDate, checkOutDate, priceRange, currency, hotelName, ratings };

        const { data: hotelRes } = await amadeus.shopping.hotelOffers.get(reqParams)
            .catch(err=>console.error(err));

        const hotels = []
        hotelRes.forEach(element => {
            const newHotel = filterHotelProps(element);
            hotels.push(newHotel);
        });

        res.json(hotels);
    },

    async getHotelOffers(req, res) {
        const { hotelId, checkInDate, checkOutDate } = req.query;
        const reqParams = { hotelId, checkInDate, checkOutDate };

        const { data: hotel } = await amadeus.shopping.hotelOffersByHotel.get(reqParams)
            .catch(err=>console.error(err));

        const offers = filterHotelProps(hotel)
        res.json(offers);
    }
}
