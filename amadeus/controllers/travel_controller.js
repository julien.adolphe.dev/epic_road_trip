var AmadeusApk = require('amadeus');

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;

var amadeus = new AmadeusApk({
    clientId: CLIENT_ID,
    clientSecret: CLIENT_SECRET
});

function filterFlightProperties(flight){
    const {id, itineraries, price} = flight;
    resObject = {id, itineraries, price};
    return resObject;
}

module.exports = {

    async getFlights (req, res) {
        const {originLocationCode, destinationLocationCode, departureDate, adults = 1, nonStop = false, maxPrice, returnDate, max, travelClass} = req.query
        requestParams = {originLocationCode, destinationLocationCode, departureDate, adults, nonStop, maxPrice, returnDate, max, travelClass};
    
        const {data: flightRes} = await amadeus.shopping.flightOffersSearch.get(requestParams)
        .catch(err=>console.error(err));

        flights = []
        flightRes.forEach(element => {
            newFlight = filterFlightProperties(element);
            flights.push(newFlight);
        });
    
        res.json(flights);
    }
};