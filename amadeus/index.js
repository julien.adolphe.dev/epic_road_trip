const express = require("express");
const cors = require("cors")

const PORT = 8001;
const HOST = '0.0.0.0';

const app = express();

const corsOptions = {
    origin: "*"
};

app.use(cors(corsOptions));

const dotenv = require('dotenv');
dotenv.config();

const { handleError } = require('./helpers/error');

const enjoy = require('./routes/enjoy');
const sleep = require('./routes/sleep');
const travel = require('./routes/travel');

app.get("/", (req, res) => {
    res.send("Hello from AMADEUS MICROSERVICE \n");
});

//Middleware
app.use(express.json());

//Route Middlewares
app.use('/api/v1/enjoy', enjoy);
app.use('/api/v1/sleep', sleep);
app.use('/api/v1/travel', travel);

app.use((err, req, res, next) => {
    handleError(err, res);
});

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});
