# Epic Road Trip

## Getting Started

#### /!\ Consider you have `docker` & `docker-compose` installed /!\

### First time Build :

```
git clone https://gitlab.com/julien.adolphe.dev/epic_road_trip
cd epic_road_trip

# Run npm install at the root will run it in each service
npm install # OR yarn

docker-compose up --build
# Wait everything is running
# Stop the docker with Ctrl + C OR docker-compose stop
docker-compose up
```

### Apps ports

```
You can see the different apps containers at these ports :
- U.I -> localhost:3000
- Server -> localhost:8000 (main server and auth)
- Amadeus API microservices (enjoy, sleep, travel) -> localhost:8001
- Foursquare API microservices (drink, eat) -> localhost:8002
- Maps visualization -> localhost:8004
- Swagger API doc -> localhost:8000/api-docs
- mongoDB emits on port 27017
```

### Database possible errors

```
# If bind 27017 error
# Check mongo port and kill the process

sudo netstat -pna | grep 27017
sudo kill <processid>

# If unable to open WiredTiger
# Change permissions in root folder
sudo chown -R $USER:$USER .
```

### For iOS user's

```
# In order to prevent error : process.dlopen(module, path.toNamespacedPath(filename))

docker exec server npm rebuild bcrypt --build-from-source
```
